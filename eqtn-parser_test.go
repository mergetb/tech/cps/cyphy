package cyphy

import (
	"testing"
)

func TestParseEquation0(t *testing.T) {

	src := "e = mc^2"
	eqtn, err := ParseEquation(src)
	if err != nil {
		t.Fatal(err)
	}
	if eqtn == nil {
		t.Fatal("equation is nil")
	}

}
