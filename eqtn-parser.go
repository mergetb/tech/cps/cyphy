package cyphy

/*
 * Derived from
 * https://github.com/alecthomas/participle/blob/master/_examples/expr/main.go
 */

import (
	"fmt"
	"strings"

	"github.com/alecthomas/participle"
	"github.com/alecthomas/participle/lexer"
	"github.com/alecthomas/participle/lexer/ebnf"
)

type Operator int

const (
	OpMul Operator = iota
	OpDiv
	OpAdd
	OpSub
)

var operatorMap = map[string]Operator{"+": OpAdd, "-": OpSub, "*": OpMul, "/": OpDiv}

func (o *Operator) Capture(s []string) error {
	*o = operatorMap[s[0]]
	return nil
}

type Intrinsic struct {
	Name string      `@("sin" | "cos" | "tan")`
	Arg  *Expression `"(" @@ ")"`
}

type Value struct {
	Number        *float64    `  @(Number)`
	Intrinsic     *Intrinsic  `| @@`
	Derivative    *string     `| @Ident"'"`
	Variable      *string     `| @Ident`
	Subexpression *Expression `| "(" @@ ")"`
}

type Factor struct {
	Base     *Value `@@`
	Exponent *Value `[ "^" @@ ]`
}

type OpFactor struct {
	Operator Operator `@("*" | "/")`
	Factor   *Factor  `@@`
}

type Term struct {
	Left  *Factor     `@@`
	Right []*OpFactor `{ @@ }`
}

type OpTerm struct {
	Operator Operator `@("+" | "-")`
	Term     *Term    `@@`
}

type Expression struct {
	Left  *Term     `@@`
	Right []*OpTerm `{ @@ }`
}

type Equation struct {
	Left  *Expression `@@"="`
	Right *Expression `@@`
}

// Display

func (i *Intrinsic) String() string {

	return fmt.Sprintf("%s(%s)", i.Name, i.Arg.String())

}

func (o Operator) String() string {
	switch o {
	case OpMul:
		return "*"
	case OpDiv:
		return "/"
	case OpSub:
		return "-"
	case OpAdd:
		return "+"
	}
	panic("unsupported operator")
}

func (v *Value) String() string {
	if v.Number != nil {
		return fmt.Sprintf("%g", *v.Number)
	}
	if v.Intrinsic != nil {
		return v.Intrinsic.String()
	}
	if v.Derivative != nil {
		return *v.Derivative + "'"
	}
	if v.Variable != nil {
		return *v.Variable
	}
	return "(" + v.Subexpression.String() + ")"
}

func (f *Factor) String() string {
	out := f.Base.String()
	if f.Exponent != nil {
		out = fmt.Sprintf("pow(%s, %s)", f.Base.String(), f.Exponent.String())
	}
	return out
}

func (o *OpFactor) String() string {
	return fmt.Sprintf("%s %s", o.Operator, o.Factor)
}

func (t *Term) String() string {
	out := []string{t.Left.String()}
	for _, r := range t.Right {
		out = append(out, r.String())
	}
	return strings.Join(out, " ")
}

func (o *OpTerm) String() string {
	return fmt.Sprintf("%s %s", o.Operator, o.Term)
}

func (e *Expression) String() string {
	out := []string{e.Left.String()}
	for _, r := range e.Right {
		out = append(out, r.String())
	}
	return strings.Join(out, " ")
}

var cylexer = lexer.Must(ebnf.New(`
		Ident = (alpha | "_") { "_" | "." | alpha | digit } .
		String = "\"" { "\u0000"…"\uffff"-"\""-"\\" | "\\" any } "\"" .
		Number = [ "-" | "+" ] ("." | digit) { "." | digit } .
		Punct = "!"…"/" | ":"…"@" | "["…` + "\"`\"" + "| \"'\"" + ` | "{"…"~" .
		Whitespace = ( " " | "\t" ) { " " | "\t" } .
		alpha = "a"…"z" | "A"…"Z" | "α"…"ω" | "Α"…"Ω"  .
		digit = "0"…"9" .
		any = "\u0000"…"\uffff" .
	`))

func ParseEquation(src string) (*Equation, error) {

	parser, err := participle.Build(
		&Equation{},
		participle.Lexer(cylexer),
		participle.Elide("Whitespace"),
	)
	if err != nil {
		return nil, err
	}

	eqtn := &Equation{}
	err = parser.ParseString(src, eqtn)
	if err != nil {
		return nil, err
	}

	return eqtn, nil

}
