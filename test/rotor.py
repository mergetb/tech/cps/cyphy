import json
import mergexp as mx
from mergexp.mechanical import Rotor
from mergexp.unit import hz

net = mx.Topology('rotor')

r = net.phyo(Rotor, 'r', H=25)
a = net.actuator('a', r.τ)
s = net.sensor('s', var = r.ω, rate = hz(100), target = 'c')
c = net.device('c')

lan = net.connect([a, s, c])
lan[a].ip.addrs = ['10.0.0.1/24']
lan[s].ip.addrs = ['10.0.0.2/24']
lan[c].ip.addrs = ['10.0.0.3/24']

print(json.dumps(net.xir(), indent=2, ensure_ascii=False))
